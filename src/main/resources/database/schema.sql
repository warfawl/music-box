DROP TABLE IF EXISTS `music_box`.`likes`;
DROP TABLE IF EXISTS `music_box`.`tracks`;
DROP TABLE IF EXISTS `music_box`.`albums`;
DROP TABLE IF EXISTS `music_box`.`users`;

CREATE TABLE IF NOT EXISTS `music_box`.`users` (
  `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL UNIQUE,
  `nickname` VARCHAR(255) NOT NULL UNIQUE,
  `password` VARCHAR(255) NOT NULL,
  `role` ENUM('ADMIN', 'USER') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `music_box`.`albums` (
  `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
  `fk_user_id` BIGINT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  CONSTRAINT UNIQUE (`fk_user_id`, `title`),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`fk_user_id`) REFERENCES `music_box`.`users`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `music_box`.`tracks` (
  `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
  `fk_album_id` BIGINT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  CONSTRAINT UNIQUE (`fk_album_id`, `title`),
  PRIMARY KEY (`id`),
  FOREIGN KEY (`fk_album_id`) REFERENCES `music_box`.`albums`(`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `music_box`.`likes` (
  `fk_user_id` BIGINT NOT NULL,
  `fk_track_id` BIGINT NOT NULL,
  PRIMARY KEY (`fk_user_id`, `fk_track_id`),
  FOREIGN KEY (`fk_user_id`) REFERENCES `music_box`.`users`(`id`),
  FOREIGN KEY (`fk_track_id`) REFERENCES `music_box`.`tracks`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


