package ua.danit.musicbox.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.danit.musicbox.model.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

}
