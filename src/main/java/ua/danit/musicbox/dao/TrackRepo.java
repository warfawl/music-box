package ua.danit.musicbox.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.danit.musicbox.model.entity.Track;

@Repository
public interface TrackRepo extends JpaRepository<Track, Long> {
}
