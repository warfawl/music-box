package ua.danit.musicbox.services;

import ua.danit.musicbox.model.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();
    User findById(Long id);
    User add(User user);
    void remove(Long id);

}
