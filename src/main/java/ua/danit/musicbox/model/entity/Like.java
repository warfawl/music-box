package ua.danit.musicbox.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "likes")
public class Like implements Serializable {

    @EmbeddedId
    private LikePK likePK;


    @Embeddable
    public class LikePK implements Serializable {

        @OneToOne
        @JoinColumn(name = "fk_user_id")
        User user;

        @OneToOne
        @JoinColumn(name = "fk_track_id")
        Track track;

        public LikePK(User user, Track track) {
            this.user = user;
            this.track = track;
        }

        public LikePK() {

        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LikePK likePK = (LikePK) o;
            return Objects.equals(user, likePK.user) &&
                    Objects.equals(track, likePK.track);
        }

        @Override
        public int hashCode() {

            return Objects.hash(user, track);
        }
    }
}
